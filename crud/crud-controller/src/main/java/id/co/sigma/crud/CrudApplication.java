package id.co.sigma.crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

import feign.RequestInterceptor;
import id.co.sigma.crud.CrudApplication;
import id.co.sigma.crud.configuration.FeignInterceptor;

@SpringBootApplication
@EnableAutoConfiguration
@EnableFeignClients
@ConfigurationProperties
@EnableDiscoveryClient
public class CrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudApplication.class, args);
	}
	
	@Bean
	public RequestInterceptor getUserFeignClientInterceptor() {
		return new FeignInterceptor();
	}

}
