package id.co.sigma.crud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import id.co.sigma.crud.feign.MinicoreInterface;
import id.co.sigma.crud.model.Customer;
import id.co.sigma.crud.response.MessageResponse;

@Service
public class CustomerService {


	@Autowired
	private MinicoreInterface minicoreInterface;

	public List<Customer> findAll() {

		return minicoreInterface.getAll();

	}

	public MessageResponse add(Customer customer) {
		return minicoreInterface.addCustomer(customer);
	}
	
	public MessageResponse update(Customer customer) {
		return minicoreInterface.updateCustomer(customer);
	}
	
	public List<Customer> deleteById(Long id){
		return minicoreInterface.deleteByIdCustomer(id);
	}
	
	public Customer getByIdCustomer(Long id) {
		return minicoreInterface.getByIdCustomer(id);
	}

}