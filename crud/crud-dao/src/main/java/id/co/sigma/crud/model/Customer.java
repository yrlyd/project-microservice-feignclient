package id.co.sigma.crud.model;

//AUTHOR : YURI LIADI & KHAIRIL HARIANTO
//DATE : 23/06/2019

import java.util.Set;

import id.co.sigma.crud.model.Account;

//@Entity
//@Table(name = "customer")
public class Customer {

//	@Id
//	@Column(name = "id_customer")
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idCustomer;

//	@Column(name = "name", nullable = false, unique = false, length = 10)
	private String name;

//	@Column(name = "phone_number", nullable = true, unique = true, length = 13)
	private String phoneNumber;

//	@Column(name = "address", nullable = true, unique = false, length = 100)
	private String address;

//	@OneToMany(fetch = FetchType.LAZY, mappedBy = "idCustomer")
	private Set<Account> account;

	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Customer(Long idCustomer, String name, String phoneNumber, String address, Set<Account> account) {
		super();
		this.idCustomer = idCustomer;
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.account = account;
	}

	public Long getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(Long idCustomer) {
		this.idCustomer = idCustomer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Set<Account> getAccount() {
		return account;
	}

	public void setAccount(Set<Account> account) {
		this.account = account;
	}

}
