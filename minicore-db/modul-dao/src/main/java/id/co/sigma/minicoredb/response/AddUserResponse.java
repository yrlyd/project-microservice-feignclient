package id.co.sigma.minicoredb.response;

public class AddUserResponse extends BaseResponse {

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
