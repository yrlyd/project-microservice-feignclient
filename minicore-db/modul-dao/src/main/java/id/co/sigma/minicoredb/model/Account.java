package id.co.sigma.minicoredb.model;

import java.io.Serializable;

//AUTHOR : YURI LIADI & KHAIRIL HARIANTO
//DATE : 23/06/2019

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "account")
public class Account implements Serializable {

	@Id
	@Column(name = "id_account")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idAccount;

	@Column(name = "number", nullable = false, unique = true, length = 10)
	private String number;

	@Column(name = "balance")
	private BigDecimal balance;

	@JsonIgnore
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "id_customer")
	private Customer idCustomer;

	public Account(Long idAccount, String number, BigDecimal balance, Customer idCustomer) {
		super();
		this.idAccount = idAccount;
		this.number = number;
		this.balance = balance;
		this.idCustomer = idCustomer;
	}

	public Account() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getIdAccount() {
		return idAccount;
	}

	public void setIdAccount(Long idAccount) {
		this.idAccount = idAccount;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public Customer getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(Customer idCustomer) {
		this.idCustomer = idCustomer;
	}

}
