package id.co.sigma.minicoredb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableAutoConfiguration
@ConfigurationProperties
@EnableDiscoveryClient
@EnableCaching
public class MinicoreDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(MinicoreDbApplication.class, args);
	}

}
