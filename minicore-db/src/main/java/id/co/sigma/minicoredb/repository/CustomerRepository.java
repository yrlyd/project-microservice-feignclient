package id.co.sigma.minicoredb.repository;

//AUTHOR : YURI LIADI & KHAIRIL HARIANTO
//DATE : 23/06/2019

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import id.co.sigma.minicoredb.model.Customer;

@Service
public interface CustomerRepository extends JpaRepository<Customer, Long>{

	

}
