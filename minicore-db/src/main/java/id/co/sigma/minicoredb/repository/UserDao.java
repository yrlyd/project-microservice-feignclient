package id.co.sigma.minicoredb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.sigma.minicoredb.entity.Users;

public interface UserDao extends JpaRepository<Users, Long> {
    Users findByUsernameAndPassword(String username, String password);

    Users findByUsername(String username);
}
