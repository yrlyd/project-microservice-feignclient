package id.co.sigma.minicoredb.controller;

// AUTHOR : YURI LIADI & KHAIRIL HARIANTO
// DATE : 23/06/2019

import java.math.BigDecimal;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.sigma.minicoredb.model.Account;
import id.co.sigma.minicoredb.request.DepositRequest;
import id.co.sigma.minicoredb.request.TransferRequest;
import id.co.sigma.minicoredb.request.WithdrawalRequest;
import id.co.sigma.minicoredb.response.DepositResponse;
import id.co.sigma.minicoredb.response.MessageResponse;
import id.co.sigma.minicoredb.response.WithdrawalResponse;
import id.co.sigma.minicoredb.service.AccountService;
import id.co.sigma.minicoredb.service.CustomerService;

@RestController
public class TransactionController {

	@Autowired
	AccountService serviceAccount;
	@Autowired
	CustomerService serviceCustomer;
	
	@RequestMapping(path = "transaction/deposit")
	public DepositResponse deposit(@RequestBody DepositRequest request) {
		
		Account account = serviceAccount.findByNumber(request.getNumber());
		BigDecimal balance = account.getBalance();
		BigDecimal finalBalance;
		finalBalance = balance.add(new BigDecimal(request.getAmount()));
		
		account.setBalance(finalBalance);
		
		serviceAccount.save(account);
		
		DepositResponse response = new DepositResponse();
		
		response.setBalance(balance.toString());
		response.setNumber(account.getNumber());
		
		return response;
	}
	
	@RequestMapping(path = "transaction/withdrawal")
	public WithdrawalResponse withdrawal(@RequestBody WithdrawalRequest request) {
		Account account = serviceAccount.findByNumber(request.getNumber());
		
		BigDecimal balance = account.getBalance();
		BigDecimal finalBalance;
		finalBalance = balance.subtract(new BigDecimal(request.getAmount()));
		
		account.setBalance(finalBalance);
		
		serviceAccount.save(account);
		
		WithdrawalResponse response = new WithdrawalResponse();
		
		response.setBalance(balance.toString());
		response.setNumber(account.getNumber());
		
		return response;
	}
	
	@Transactional
	@RequestMapping(path = "transaction/transfer")
	public ResponseEntity<Object> transfer(@RequestBody TransferRequest request) {
		
		String errorMsg;
		String from, to;

		try {
			from = request.getFromNumber();
			to = request.getToNumber();
			
			Account fromAccount = serviceAccount.findByNumber(from);
			Account toAccount = serviceAccount.findByNumber(to);
			
			BigDecimal amount = new BigDecimal(request.getAmount());
			
			BigDecimal fromBalance = fromAccount.getBalance();
			BigDecimal toBalance = toAccount.getBalance();
			
			BigDecimal finalFrom, finalTo;
			
			finalFrom = fromBalance.subtract(amount);
			finalTo = toBalance.add(amount);
			
			fromAccount.setBalance(finalFrom);
			toAccount.setBalance(finalTo);
			
			serviceAccount.save(fromAccount);
			serviceAccount.save(toAccount);
			errorMsg = "SUCCESS";
			MessageResponse message = new MessageResponse(errorMsg);
			return new ResponseEntity<Object>(message, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (DataIntegrityViolationException ex) {
			errorMsg = "FAILED";
			MessageResponse message = new MessageResponse(errorMsg);
			return new ResponseEntity<Object>(message, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (NullPointerException e) {
			errorMsg = "ERROR";
			MessageResponse message = new MessageResponse(errorMsg);
			return new ResponseEntity<Object>(message, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		

	}
	
	
}
