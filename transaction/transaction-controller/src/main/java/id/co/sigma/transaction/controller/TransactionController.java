package id.co.sigma.transaction.controller;

import java.util.ArrayList;
import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.co.sigma.transaction.model.Customer;
import id.co.sigma.transaction.request.DepositRequest;
import id.co.sigma.transaction.request.TransferRequest;
import id.co.sigma.transaction.request.WithdrawalRequest;
import id.co.sigma.transaction.response.DepositResponse;
import id.co.sigma.transaction.response.MessageResponse;
import id.co.sigma.transaction.response.WithdrawalResponse;
import id.co.sigma.transaction.service.CustomerService;

@RestController
public class TransactionController {

	@Autowired
	private CustomerService service;
	
	@RequestMapping(path = "transaction/deposit")
	public DepositResponse deposit(@RequestBody DepositRequest request) {
		return service.deposit(request);
	}
	
	@RequestMapping(path = "transaction/transfer")
	public ResponseEntity<Object> transfer(@RequestBody TransferRequest request){
		return service.transfer(request);
	}
	@RequestMapping(path = "transaction/withdrawal")
	public WithdrawalResponse withdrawal(@RequestBody WithdrawalRequest request) {
		return service.withdrawal(request);
	}
	
	
}
